#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

ifstream in("automat.in");

class automat
{
    public:
        int **m;
        int l;
        int col;
        int init;
        int nrFin;
        int *fin;
        int tranz;
        char *alfabet;
        void read();
        bool accept(char *cuv);
    automat();
};

automat::automat()
{
    in>>l;
    in>>col;

    alfabet=new char[l];
    char c;
    for(int i=0; i<col; i++)
    {
        in>>c;
        alfabet[i]=c;
    }
    alfabet[col]='\0';

    in>>init;
    in>>nrFin;
    fin=new int[nrFin];

    for(int i=0; i<nrFin; i++)
    {
        in>>fin[i];
    }

    tranz=0;

    m=new int*[l];
    for(int i=0; i<l; i++)
    {
        m[i]=new int[col];
        for(int j=0; j<col; j++)
            m[i][j]=-1;
    }

}

void automat::read()
{
    cout<<"Introduceti numarul de tranzitii, iar apoi tranzitiile: ";
    in>>tranz;
    int stIn, stFin, cT; //stare initiala, finala si caracterul transpus in cifre
    char c;

    for(int i=0; i<tranz; i++)
    {
        in>>stIn>>stFin;
        in>>c;

        char *poz=strchr(alfabet, c);
        if(poz)
        {
            cT=poz-alfabet; //transpunerea in cifre
        }
        m[stIn][cT]=stFin; //indicii alfabetului sunt indicii coloanelor matricei
        cout<<"st in: "<<stIn<<" col literei: "<<cT<<" st urm: "<<m[stIn][cT];
    }

    for(int i=0; i<l; i++)
    {
        for(int j=0; j<col; j++)
            cout<<m[i][j]<<" ";
        cout<<"\n";
    }
}

bool automat::accept(char *cuv)
{
    int i=0, stCur, stFin, lit;

    stCur=init;

    while(cuv[i])
    {
        bool litValida=false;

        char *poz=strchr(alfabet, cuv[i]);
        if(poz)
        {
            litValida=true;
            lit=poz-alfabet; //transpunerea in cifre
        }

        if(!litValida) //daca litera nu este in alfabet
            return false;

        if( m[stCur][lit]==-1 ) //tranzitie nedefinita
            return false;

        stCur=m[stCur][lit];
        i++;
    }

    for(int j=0; j<nrFin; j++)
        if( fin[j]==stCur ) return true;

    return false;
}

int main()
{
    cout<<"Introduceti numarul de stari, cardinalul alfabetului, alfabetul, starea initiala, numarul de stari finale si starile finale: ";
    automat graf;
    graf.read();

    if(graf.accept("a")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("b")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("aa")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("d")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("ad")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("acd")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("aabacabbbbbbbaabaabaab")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;

    if(graf.accept("ac")) cout<<"ye."<<endl;
    else cout<<"nope."<<endl;
    return 0;
}
